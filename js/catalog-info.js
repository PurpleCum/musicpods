'use strict';
(function () {


var catalogInfo = {

	'rockspace-eb10' : {
		price : '3650',
	},

	'rockspace-eb30' : {
		price : '3790',
	},

	'rockspace-eb50' : {
		price : '3990',
	},

	'ovevo-q62' : {
		price : '4190',
	},

	'meizu-pop-tw50' : {
		price : '5990',
	},

	'qcy-t1-pro' : {
		price : '3030',
	},

	'qcy-q29' : {
		price : '2450',
	},

	'syllable-d9x-tws' : {
		price : '3750',
	},

	'airpods' : {
		price : '11490',
	},

	'gear' : {
		price : '10340',
	},
};

var catalogItems = Array.from(document.querySelectorAll('.catalog-item'));
var saleKef = 0.8;
catalogItems.forEach( function(el){


	el.dataset.price = catalogInfo[el.id].price;
	el.querySelector('.catalog-item__price--del').dataset.priceDel = Math.ceil( catalogInfo[el.id].price / saleKef );
	el.querySelector('.catalog-item__price--del').innerHTML = el.querySelector('.catalog-item__price--del').dataset.priceDel.replace(/(\d{1,3})(?=((\d{3})*)$)/g, " $1") + ' Руб.';
	el.querySelector('.catalog-item__price--price').innerHTML = catalogInfo[el.id].price.replace(/(\d{1,3})(?=((\d{3})*)$)/g, " $1") + ' Руб.';

	if(el.dataset.sale !== 'sale'){
		// console.log(el);
		el.querySelector('.catalog-item__price--del').classList.add('__d-none');
	}


});


})(); // the end