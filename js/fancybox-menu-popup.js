'use strict';
(function () {

	$('#menu__callback-btn').on('click', function() {
	
	  $.fancybox.open({
	    src  : '#menu-callback-popup',
	    type : 'inline',
		touch : {
			vertical : false,  // Allow to drag content vertically
			// momentum : false   // Continuous movement when panning
		},
	
	  });
	
	});

})();
// end