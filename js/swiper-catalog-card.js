'use strict';
(function () {


var container = document.querySelector('.catalog');
var descWidth = 360.1;

var swiperWrapper = document.querySelector('#swiper-wrapper');
var swiperSlide = Array.from(document.querySelectorAll('.catalog-item'));

var swiperCatalogCard = new Swiper('.catalog', {

	init : false,
	
	slidesPerView : 1,
	spaceBetween : 30,
	speed: 500,
	effect : 'slide',
	
	loop: true,
	// бесконечная прокрутка
	
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	
	on : {
	
		resize: function(){
	
			if(+container.offsetWidth > descWidth) {
	
				swiperWrapper.classList.add('g');
	
				swiperSlide.forEach( function(el){
					el.classList.remove('swiper-slide');
				});
	
				swiperCatalogCard.destroy();
				console.log('resize');
			}
		}
	},
	
});

if(+container.offsetWidth < descWidth){

	swiperWrapper.classList.remove('g');

	swiperSlide.forEach( function(el){
		el.classList.add('swiper-slide');
	});

	swiperCatalogCard.init();
}



window.addEventListener('resize', function(){


	if(+container.offsetWidth < descWidth){
	
		swiperWrapper.classList.remove('g');
		swiperSlide.forEach( function(el){
			el.classList.add('swiper-slide');
		});
	
		var swiperCatalogCard = new Swiper('.catalog', {
		
		    slidesPerView : 1,
			spaceBetween : 30,
		    speed: 500,
		    effect : 'slide',
	
		    loop: true,
		    // бесконечная прокрутка
	
		    navigation: {
		      nextEl: '.swiper-button-next',
		      prevEl: '.swiper-button-prev',
		    },
		
			on : {
	
				resize: function(){
	
					if(+container.offsetWidth > descWidth) {
	
						swiperWrapper.classList.add('g');
	
						swiperSlide.forEach( function(el){
							el.classList.remove('swiper-slide');
						});
	
						swiperCatalogCard.destroy();
						console.log('resize');
					}
	
				}
	
			},
	
		});
	
	}
	
});





})(); // the end