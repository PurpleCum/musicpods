'use strict';
(function () {


	const filterMenu = document.querySelector('.filter');
	var filterItems = Array.from(document.querySelectorAll('.filter-item'));
	var catalogCards = Array.from(document.querySelectorAll('.catalog-item'));

	var chosenBrand = 'all';

	filterMenu.addEventListener('click', function(e){

		if(e.target.classList.contains('filter-item')) {

			removeActive();
			e.target.classList.add('__active');

			if(e.target.id != 'all') {

				catalogCards.forEach( function(el){

					if(el.dataset.brand != e.target.id) {
						el.classList.add('__hide');
					} else {
						el.classList.remove('__hide');
					}
				});

			} else {
				catalogCards.forEach( function(el){el.classList.remove('__hide')})
			}
		}

	});

	filterMenu.addEventListener('mouseover', function(e){

		if(e.target.classList.contains('filter-item')) {

			catalogCards.forEach( function(el){

				if(el.dataset.brand == e.target.id) {
					el.classList.add('__box-shadow');
				}

			});
		}

	});

	filterMenu.addEventListener('mouseout', function(e){

		catalogCards.forEach( function(el){
			el.classList.remove('__box-shadow');
		});

	});


	function removeActive(){

		filterItems.forEach( function(el){

			el.classList.remove('__active');

		});

	};

})(); // the end