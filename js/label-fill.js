'use strict';
(function () {

var inputField = Array.from(document.querySelectorAll('.input-field'));

inputField.forEach(function(el) {
    el.querySelector('input').addEventListener('change', change);
    el.querySelector('input').addEventListener('blur', change);

});

function change(e) {

	if(e.target.value) {
		e.target.parentNode.classList.add('label--filled');

	} else {
		e.target.parentNode.classList.remove('label--filled');
	}

};


})(); // the end